import React, {Suspense, lazy} from 'react'; //useState

import Intro from './components/intro';

const Map = lazy(()=>import('./components/map'));

export default function App() {	
	return (
		<div>
			<Intro />
			<Suspense fallback={<div />}>
			<Map />
			</Suspense>
		</div>
	);
} //end export default
