import React from 'react'; //useState
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import './basecamp.css';
import ReactDOM from 'react-dom';
import Jalur from './jalur';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-202794178-1');
ReactGA.pageview(window.location.pathname);

const Basecamp = (map) => {
	const center = [109.969409, -7.318774];
	const sHeight = document.documentElement.clientHeight;
	const sWidt = document.documentElement.clientWidth;
	let bearing, zoom, xAxis, yAxis, featured, posit;
	
	if (sHeight < sWidt){
		bearing = 45;
	}else{
		bearing = 0;
	}
	
	if (sWidt <= 600){
		zoom = 10;
		xAxis = 0;
		yAxis = 5*(window.innerWidth / 250000.0);
	}else if (sWidt <= 960 && sWidt > 600){
		zoom = 11;
		xAxis = window.innerWidth / 250000.0;
		yAxis = 0;
	}else{
		zoom = 11.5;
		xAxis = window.innerWidth / 250000.0;
		yAxis = 0;
	}
		map.current.on('load', () => {
			map.current.addSource('basecamp', {
				'type': 'geojson',
				'data': '/file.geojson'
			});
			map.current.addLayer({
				'id': 'BC',
				'type': 'symbol',
				'source': 'basecamp',
				'layout': {
					'icon-image': [
						'match',
						['get', 'status'],
						'TUTUP', 'icon1',
						'icon2'],
					'icon-size': 0.6,
					'icon-allow-overlap': true,
					'icon-anchor': 'bottom',
					'icon-rotate' : 0,
					'visibility': 'visible'
				}
			});			
			
			// Change the cursor to a pointer when the mouse is over the places layer.
			map.current.on('mouseenter', 'BC', function () {
				map.current.getCanvas().style.cursor = 'pointer';
			});
			// Change it back to a pointer when it leaves.
			map.current.on('mouseleave', 'BC', function () {
				map.current.getCanvas().style.cursor = '';
			});
			
			// Change the cursor to a pointer when the mouse is over the places layer.
			map.current.on('mouseenter', 'base', function () {
				map.current.getCanvas().style.cursor = 'pointer';
			});
			// Change it back to a pointer when it leaves.
			map.current.on('mouseleave', 'base', function () {
				map.current.getCanvas().style.cursor = '';
			});
			
			// Change the cursor to a pointer when the mouse is over the places layer.
			map.current.on('mouseenter', 'pos', function () {
				map.current.getCanvas().style.cursor = 'pointer';
			});
			// Change it back to a pointer when it leaves.
			map.current.on('mouseleave', 'pos', function () {
				map.current.getCanvas().style.cursor = '';
			});
			
			map.current.on('click', (e) => {
				const features = map.current.queryRenderedFeatures(e.point, { layers: ['BC'] });
				if (map.current.getLayer('base')) {
					featured = map.current.queryRenderedFeatures(e.point, { layers: ['base'] });
				}else {
					featured = [];
				}
				/*if (map.current.getLayer('jalur')) {
					lineJalur = map.current.queryRenderedFeatures(e.point, { layers: ['jalur'] });
				}*/
				if (map.current.getLayer('pos')) {
					posit = map.current.queryRenderedFeatures(e.point, { layers: ['pos'] });
				}else{
					posit = [];
				}

				if (features.length){
					let idBC = features[0].properties.id;
					let centered = features[0].geometry.coordinates;
					let target = mapboxgl.LngLat.convert(centered);
					map.current.setLayoutProperty('BC', 'icon-anchor',
						[
							'match',
							['get','id'], // get the feature id (make sure your data has an id set or use generateIds for GeoJSON sources
							idBC, 
							'top', //image when id is the clicked feature id
							'bottom' // default
						]
					);
					map.current.setLayoutProperty('BC', 'icon-rotate',
						[
							'match',
							['get','id'], // get the feature id (make sure your data has an id set or use generateIds for GeoJSON sources
							idBC, 
							180, //image when id is the clicked feature id
							0 // default
						]
					);
					map.current.setLayoutProperty('BC', 'icon-size',
						[
							'match',
							['get','id'], // get the feature id (make sure your data has an id set or use generateIds for GeoJSON sources
							idBC, 
							1, //image when id is the clicked feature id
							0.6 // default
						]
					);
					//go to click coordinate
					map.current.flyTo({
						center: [target.lng - xAxis, target.lat + yAxis],
						zoom: 14,
						bearing: 0,
						speed: 1
					});
					document.getElementById('legend').style.display = 'none';
					document.getElementById('shortInfo').style.display = 'block';
					//get BC properties
					let nama = features[0].properties.nama;
					let cluster = features[0].properties.cluster;
					let status = features[0].properties.status;
					let jalur = features[0].properties.jalur;
					let estimasi = features[0].properties.estimasi;
					let data = features[0].properties.dataBC;
					
					let content = '<div class="title">'+nama+'<br>('+cluster+')</div>';
					content += '<div class="status" id="stat">'+status+'</div>';
					content += '<div class="info"><p>Panjang Jalur : '+jalur+'</p>';
					content += '<p> Estimasi Pendakian : '+estimasi+'</p></div>';
					document.getElementById('infokus').innerHTML = content;
					
					if (status === 'BUKA'){
						document.getElementById('stat').style.backgroundColor = 'DarkTurquoise';
					}else{
						document.getElementById('stat').style.backgroundColor = 'orange';
					}
					
					ReactDOM.render(Jalur(idBC, map, data),document.getElementById('infoJalur'),() => {
						ReactGA.event({
							category: cluster,
							action: nama
						});
					});
					
				}else if (featured.length){
						console.log("di klik basecamp");
				}else if (posit.length){
						console.log("di klik pos");
				}else{
					map.current.flyTo({
						center: center,
						zoom: zoom,
						bearing: bearing,
						speed: 1
					});
					map.current.setLayoutProperty('BC', 'icon-rotate', 0);
					map.current.setLayoutProperty('BC', 'icon-size', 0.6);
					map.current.setLayoutProperty('BC', 'icon-anchor', 'bottom');
					map.current.setLayoutProperty('BC', 'visibility', 'visible');
					
					document.getElementById('legend').style.display = 'block';
					document.getElementById('shortInfo').style.display = 'none';
					document.getElementById('infoJalur').style.display = 'block';
					
					if (map.current.getLayer('jalur')) map.current.removeLayer('jalur');
					if (map.current.getLayer('pos')) map.current.removeLayer('pos');
					if (map.current.getLayer('base')) map.current.removeLayer('base');
					if (map.current.getSource('jalur')) map.current.removeSource('jalur');
				}
			});
		});//end load
		return (
			<div className="sidebar" id="shortInfo">
				<div id="infokus"></div>
				<div id="infoJalur" />
			</div>
		);
}
export default Basecamp;
