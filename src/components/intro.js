import React from 'react'; 
import './intro.css';

const Intro = () => {
	function hideIntro() {
		document.getElementById("myModal").style.display = "none";
	}
	return (
		<div id="myModal" className="modal">

		<div className="modal-content">
			<h1>&nbsp;Travelinfo&nbsp;</h1>
			<h3>Media informasi wisata Minat Khusus<br />wilayah Wonosobo dan sekitarnya</h3>
			<button className="modal-close" onClick={hideIntro}>Buka Peta</button>
		</div>

	</div>
	)
}
export default Intro;
