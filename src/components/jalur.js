
const Jalur = (id, map, data) => {
		function showup() {
			map.current.setLayoutProperty('BC', 'visibility', 'none');
			map.current.addLayer({
				'id': 'base',
				'type': 'symbol',
				'source': 'basecamp',
				'layout': {
					'icon-image': [
						'match',
						['get', 'status'],
						'TUTUP', 'icon1',
						'icon2'],
					'icon-size': 1,
					'icon-allow-overlap': true,
					'icon-anchor': 'bottom',
					'icon-rotate' : 0,
					'visibility': 'visible'					
				},
				'filter': ['==', 'id', id]
			});

			if (data === 'yes'){
				document.getElementById('shortInfo').style.display = 'none';
				map.current.addSource('jalur', {
					'type': 'geojson',
					'data': '/data/'+id+'.geojson'
				});
				map.current.addLayer({
					'id': 'jalur',
					'type': 'line',
					'source': 'jalur',
					'layout': {
						'line-join': 'round',
						'line-cap': 'round'
					},
					'paint': {
						'line-color': 'Yellow',
						'line-width': 5
					},
					'filter': ['==', '$type', 'LineString']
				});
				map.current.addLayer({
					'id': 'pos',
					'type': 'symbol',
					'source': 'jalur',
					'layout': {
						'icon-image': 'drawing',
						'icon-size': 0.6,
						'icon-allow-overlap': true,
						'icon-anchor': 'bottom',
						'icon-rotate' : 0				
					},
					'filter': ['==', '$type', 'Point']
				});
			}else{
				document.getElementById('infoJalur').style.display = 'none';
				let content = '<div class="title">Data Belum Tersedia</div><hr>';
				content += '<div class="info"><p>Saat ini <b>Travelinfo</b> masih dalam proses pengumpulan data GPS Tracking jalur pendakian setiap Basecamp Pendakian</p></div>';
		
				document.getElementById('infokus').innerHTML = content;
			}
			
		}
		return (
			<button id="infobtn"className="btn" onClick={showup}>LIHAT JALUR</button>
		);
}
export default Jalur;
