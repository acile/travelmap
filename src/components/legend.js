import React from "react";
import './legend.css';

const Legend = () => {
	return (
		<div id="legend">
			<img className="manImg" src="/icon2.svg" alt="*"></img> <span> BUKA </span>
			<br />
			<br />
			<img style={{ marginLeft: 5 }} className="manImg" src="/icon1.svg" alt="*"></img> <span> TUTUP</span>
		</div>
	);
}
export default Legend;
