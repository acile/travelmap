import React, { useRef, useEffect } from 'react'; //useState
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import './map.css';
import Basecamp from './basecamp';
import Legend from './legend';
import ReactDOM from 'react-dom';

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;

const Map = () => {
	const mapContainer = useRef(null);
	const map = useRef(null);
	//const BCid = useRef(null);
	const center = [109.969409, -7.318774];
	const bounds = [
		[108.75, -8], // West-South coordinates
		[111.25, -6.75] // East-North coordinates
	];
	const sHeight = document.documentElement.clientHeight;
	const sWidt = document.documentElement.clientWidth;
	//let bearing, zoom, xAxis, yAxis;
	let bearing, zoom;
	
	if (sHeight < sWidt){
		bearing = 45;
	}else{
		bearing = 0;
	}
	
	if (sWidt <= 600){
		zoom = 10;
		//xAxis = 0;
		//yAxis = 2*(window.innerWidth / 250000.0);
	}else if (sWidt <= 960 && sWidt > 600){
		zoom = 11;
		//xAxis = window.innerWidth / 250000.0;
		//yAxis = 0;
	}else{
		zoom = 11.5;
		//xAxis = window.innerWidth / 250000.0;
		//yAxis = 0;
	}
	
	useEffect(() => {
		if (map.current) return; // initialize map only once
		map.current = new mapboxgl.Map({
			container: mapContainer.current,
			style: 'mapbox://styles/acile-here/cke2zk0xm1eog19mqvd1hhczq?optimize=true',
			center: center,
			zoom: zoom,
			minZoom : 8,
			maxZoom : 16,
			maxBounds: bounds,
			pitch: 60,
			bearing: bearing, // bearing in degrees
			//attributionControl: false,
			pitchWithRotate: false,
		});
		// add navigation control (the +/- zoom buttons)
		map.current.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
		ReactDOM.render(Basecamp(map),document.getElementById('sider'));		
	}); //end useEffect

	return (
		<div>
		<div ref={mapContainer} className="map-container" />
		<div id="sider" />
		<Legend />
		
		</div>
	);
}
export default Map;
