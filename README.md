# TRAVELmap

Media informasi wisata minat khusus di wilayah wonosobo dan sekitarnya

Kunjungi https://travelinfo.vercel.app

## Fitur

- [x] Display Lokasi Basecamp Pendakian

- [x] Display Informasi Umum Basecamp Pendakian

- [ ] Display Jalur Pendakian Tiap Basecamp
- [ ] Display Informasi Lengkap Jalur Pendakian

- [ ] Filter Berdasarkan Gunung
- [ ] Filter Berdasarkan Status Basecamp Pendakian